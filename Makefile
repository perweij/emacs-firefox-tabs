SRC = firefox-tabs.el


all: update-docs


# Replace the source header documentation with the documentation of the
# README.org.
update-docs: README.org
	TMPF=$$(mktemp) && \
        sed -i '/ Commentary:/,/ Code:/ c;;; Code:' $(SRC) && \
	sed -e 's|^| |' \
            -e 's|^ \*|;|g' \
            -e 's|^[ ]*:||g' \
            -e 's|^|;;|g' \
            -e 's|;[ ]*$$|;|g' < README.org > $$TMPF && \
	sed -e "/;;; Code:/r $$TMPF" -e 'x;$$G' -e '1d' $(SRC) > $$TMPF.2 && \
	mv $$TMPF.2 $(SRC) && \
	rm -f $$TMPF $$TMPF.2
