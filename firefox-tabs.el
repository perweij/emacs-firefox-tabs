;;; firefox-tabs.el --- Firefox tab browser          -*- lexical-binding: t; -*-

;; Author: Per Weijnitz <per.weijnitz@gmail.com>
;; Keywords: outlines
;; URL: https://gitlab.com/perweij/emacs-firefox-tabs
;; Version: 0.10
;; Package-Requires: ((emacs "25.1"))

;; Copyright (C) 2019, Per Weijnitz, all rights reserved.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.



;;; Commentary:
;;
;; This is a tool for parsing all the tabs from the current
;; Firefox session and profile.
;;
;; It will produce a GNU/Emacs org-document in a buffer namned by the
;; firefox-tabs-result-buffer variable below (default "*links*").
;;
;; The top level headers correspond to tab links, and are represented by
;; org-links.  The sub headers correspond to the tab history.
;;
;; Tested with Firefox 70.
;;
;;
;;; Prerequisites:
;;
;; Firefox uses a custom lz4 format for storing files, and the program
;; depends on an external tool for unpacking this data.  Check out the
;; project below, compile it, and place the binary lz4jsoncat in your PATH.
;;
;;    <https://github.com/andikleen/lz4json.git>
;;
;;
;;
;;; Installation:
;;
;; Evaluate the elisp source file in some manner.  If you like quelpa,
;; this line would also work in your init.el:
;;
;;   (when (not (require 'firefox-tabs nil 'noerror))
;;     (quelpa '(firefox-tabs :repo "perweij/emacs-firefox-tabs" :fetcher gitlab)))
;;   (require 'firefox-tabs)
;;
;;
;;; Invokation:
;;
;; First start Firefox with the session you like to backup loaded. It should be running
;; during the backup.
;;
;; Start with:
;; (firefox-tabs-generate)
;;
;; It is also possible to supply a file directly, to skip
;; search heuristics:
;; (firefox-tabs-generate "/tmp/recovery.jsonlz4")
;;
;;; License:
;;
;; Copyright (C) 2019, Per Weijnitz, all rights reserved.
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; <https://www.gnu.org/licenses/gpl-3.0.txt>
;;
;;
;;
;; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;;; Code:

(require 'json)
(require 'seq)

;; Change this to "~/snap/firefox/common/.mozilla/firefox" if you use Snap Firefox
(defvar firefox-tabs-base-dir "~/.mozilla/firefox")

(defvar firefox-tabs-result-buffer "*links*")
(defconst firefox-tabs-debug nil)

(defun firefox-tabs-get-profile (firefox-tabs-dir)
  "Try to get the active firefox profile.
Search is based on root directory FIREFOX-TABS-DIR."
  (car
   (seq-remove #'(lambda (x) (or (string= x "Crash Reports")
                                 (string= x "Pending Pings")
                                 (string-prefix-p "." x)))
               (mapcar
                #'car
                (seq-filter #'(lambda (x) (nth 1 x))
                            (sort (directory-files-and-attributes firefox-tabs-dir)
                                  #'(lambda (x y) (time-less-p (nth 6 y) (nth 6 x)))))))))

(defun firefox-tabs-make-data-store-path (firefox-tabs-dir fname)
  "Concatenate a full path to the Firefox persisted tab data file.
Search is based on root directory FIREFOX-TABS-DIR.
Basename is given by FNAME."
  (concat (file-name-as-directory firefox-tabs-dir)
          (file-name-as-directory (firefox-tabs-get-profile firefox-tabs-dir))
          (file-name-as-directory "sessionstore-backups")
          fname))

(defun firefox-tabs-get-tab-store (firefox-tabs-dir)
  "Locate the Firefox persisted tab data file, and return.
Search is based on root directory FIREFOX-TABS-DIR."
  (let* ((name-running (firefox-tabs-make-data-store-path firefox-tabs-dir "recovery.jsonlz4"))
         (name-not-running (firefox-tabs-make-data-store-path firefox-tabs-dir "sessionstore.jsonlz4")))
    (if (file-exists-p name-running)
        name-running
      (if (file-exists-p name-not-running)
          name-not-running
        (user-error "Firefox persisted tab file not found!")))))

(defun firefox-tabs-extract-tab-data (firefox-tabs-dir &optional fname)
  "Uncompress the Firefox persisted tab data file.
Store in a temporary file, which is returned.  This function
depends on the external tool lz4jsoncat.  Search is based on root
directory FIREFOX-TABS-DIR.  Optional FNAME overrides search with
a specific file."
  (unless (executable-find "lz4jsoncat")
    (user-error "External command lz4jsoncat not found! Get here: https://github.com/andikleen/lz4json.git"))
  (let* ((temp-file (make-temp-file "json"))
         (data-file (if fname
                        (if (file-exists-p fname) fname (user-error (format "Given file '%s' does not exist." fname)))
                      (firefox-tabs-get-tab-store firefox-tabs-dir)))
         (cmd (concat "lz4jsoncat " data-file " >" temp-file)))
    (if firefox-tabs-debug
        (message (format "Running %s" cmd)))
    (shell-command cmd)
    temp-file))

(defun firefox-tabs-org-quote (str)
  "Quote the given STR to be usable in an org-link."
  (let* ((str-esc0 (replace-regexp-in-string "\\[" "{" str))
         (str-esc1 (replace-regexp-in-string "]" "}" str-esc0)))
    str-esc1))

(defun firefox-tabs-proc-entry (entry)
  "Process a tab ENTRY into an org-link."
;;  (if (cl-search "group-tab.html" (gethash "url" entry))
;;      (print "found grouptab -- fixme\n"))
  (let* ((title (firefox-tabs-org-quote (gethash "title" entry)))
         (url (firefox-tabs-org-quote (gethash "url" entry))))
    (if (and title
             (not (string-prefix-p "about:" url))
             (not (string-prefix-p "New Tab" title)))
        (concat "[[" url "][" title "]]"))))

(defun firefox-tabs-create-org (firefox-tabs-dir &optional fname)
  "Convert a Firefox tab state file into an 'org-mode' buffer.
Search is based on root directory FIREFOX-TABS-DIR.
Optional file FNAME overrides search."
  (let ((data-file (firefox-tabs-extract-tab-data firefox-tabs-dir fname)))
    (message (format "Hang on, crunching %s (temp-file)..." data-file))
    (let* ((json-object-type 'hash-table)
           (json-array-type 'list)
           (json-key-type 'string)
           (json (json-read-file data-file))
           (windows (gethash "windows" json)))

      (with-current-buffer (get-buffer-create firefox-tabs-result-buffer)
        (dolist (window windows)
          (let* ((tabs (gethash "tabs" window)))
            (dolist (tab tabs)
              (let ((tab-entries (reverse (gethash "entries" tab))))
                (if tab-entries
                    (with-current-buffer firefox-tabs-result-buffer
                      ;;(if (not (firefox-tabs-proc-entry (car tab-entries))) (debug))
                      (insert (concat "* " (firefox-tabs-proc-entry (car tab-entries)) "\n"))
                      (let* ((entries (cdr tab-entries)))
                        (dolist (entry entries)
                          (let ((link (firefox-tabs-proc-entry entry)))
                            (if link
                                (insert (concat "** " link "\n")))))))))))))
      (delete-file data-file)
      (message "Ready."))))

(defun firefox-tabs-generate (&optional fname)
  "Convert Firefox tab state file into an 'org-mode' document with org-links.
Search is based on root directory FIREFOX-TABS-DIR.
Optional file FNAME overrides search."
  (interactive)
  (firefox-tabs-create-org firefox-tabs-base-dir fname)
  (switch-to-buffer firefox-tabs-result-buffer)
  (org-mode))
;(org-show-all)

(provide 'firefox-tabs)

;; Local Variables:
;; coding: utf-8
;; End:

;;; firefox-tabs.el ends here
